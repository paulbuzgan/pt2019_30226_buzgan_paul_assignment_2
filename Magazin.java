
package ElementeMagazin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import Instrumente.*;

public class Magazin {
	
	public static ArrayList<Coada> Caserii = new ArrayList<Coada>();
	
	Timer timer ; 
	
	public static String logToShow=" ";
	
	public static int cozi_deschise;
	public static boolean op= true;
	public static int peak=0;
	public static int peaksave=0;
	static int spectime_min=0;
	static int spectime_max=0;

    //multe elemnte pentru afisarea in interfata si calularea timpilor optimi
    GeneratorClienti gen = new GeneratorClienti();
    
    static ArrayList<Client> AllClients = new ArrayList<Client>();
	
	public Magazin(int nrCozi, int asteptare_min, int asteptare_max, int procesare_min, int procesare_max, int specific_min, int specific_max)
	{
		cozi_deschise=nrCozi;
		for(int j=1; j<=10; j++)
		{
			Coada auxiliara=new Coada();
			Caserii.add(auxiliara);
		}
		
		spectime_min=specific_min;
		spectime_max=specific_max;
		
		gen.timp_spawning_min=asteptare_min;
		gen.timp_spawning_maxim=asteptare_max;
		gen.timp_procesare_min=procesare_min;
		gen.timp_procesare_maxim=procesare_max;
		creazaClienti();
		//aici e contructorul ce pune elementele in magazin si creaza clientii random de la generatorul de clienti
	}
	
	public void creazaClienti()
	{
		for(int i=0; i<20; i++)
		{
			Client aux = gen.genereaza();
			AllClients.add(aux);
		}
		
		Collections.sort(AllClients, new Sortby());
		
		for(Client a : AllClients)
		{
			System.out.println(a.nume + "\n");
		}
		//aici cream clientii si ii orodnam dupa timpul de asteptare
		
	}
	
	public static int find_min_queue()
	{
		int min=200, min_coada=1, counter=1;
		for(Coada aux : Caserii)
		{
			if(aux.nr_clienti < min && counter <= cozi_deschise)
			{
				min=aux.nr_clienti;
				min_coada=counter;
			}
			counter++;
		}
		return min_coada;	
	}
	
	//functie ce gaseste coada cu nr cel mai mic de clienti
	
	public static void distribuie()
	{
		if(Magazin.op == true) {
		for(Client a : AllClients)
		{
			if(a.timp_asteptare == TimerMagazin.timp_program)
			{
				logToShow=logToShow + TimerMagazin.timp_program + " : "+a.nume +" a intrat in Coada " + find_min_queue() + "\n";
				System.out.println(logToShow + TimerMagazin.timp_program + " : " + a.nume+" a intrat in Coada " + find_min_queue() + "\n");
				Caserii.get(find_min_queue()-1).adauga(a);
			}
		}
		}
		//metoda ce distruie clientii in functie de care coada are mai putini clienti in ea
	}
	//metoda ce porneste toate caseriile pe secunda sa vada daca au ceva de executat
	public static void StartCaserii()
	{
		if(TimerMagazin.timp_program >= 1 && Magazin.op == true)
		{
			if(Caserii.get(0).sir.isEmpty() == false)
			{
				Caserii.get(0).proceseaza();
			}
			
			if(Caserii.get(1).sir.isEmpty() == false)
			{
				Caserii.get(1).proceseaza();
			}
			
			if(Caserii.get(2).sir.isEmpty() == false)
			{
				Caserii.get(2).proceseaza();
			}
			
			if(Caserii.get(3).sir.isEmpty() == false)
			{
				Caserii.get(3).proceseaza();
			}
			
			if(Caserii.get(4).sir.isEmpty() == false)
			{
				Caserii.get(4).proceseaza();
			}
			
			if(Caserii.get(5).sir.isEmpty() == false)
			{
				Caserii.get(5).proceseaza();
			}
			
			if(Caserii.get(6).sir.isEmpty() == false)
			{
				Caserii.get(6).proceseaza();
			}
			
			if(Caserii.get(7).sir.isEmpty() == false)
			{
				Caserii.get(7).proceseaza();
			}
			if(Caserii.get(8).sir.isEmpty() == false)
			{
				Caserii.get(8).proceseaza();
			}
			
			if(Caserii.get(9).sir.isEmpty() == false)
			{
				Caserii.get(9).proceseaza();
			}
		}
	}
	
	
	
	
	
	public void Start(int max) {
	        timer = new Timer();
	        timer.schedule(new TimerMagazin(max), 0, 1*1000);  
	    }
	//metoda de pornire a timerului
	
	

}
