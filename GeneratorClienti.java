

package Instrumente;

import java.util.Random;

import ElementeMagazin.*;


public class GeneratorClienti {
	
	public int timp_spawning_min=0;
	public int timp_spawning_maxim=0;
	
	public int timp_procesare_min=0;
	public int timp_procesare_maxim=0;
	
	//aici se salveaza timpii din interfata si avem o functie de nr random intr-un interval pentru a spama clienti
	


	private static int getRandomNumber(int min, int max)
	{
		if(min>=max)
		{
			throw new IllegalArgumentException("maxim mai mic decat minim");	
		}
		
		Random r= new Random();
		return r.nextInt((max - min) +1) + min;
	}
	
	//aici se generaza clientii cu acea functie

	public Client genereaza()
	{
		Client temp = new Client(getRandomNumber(timp_spawning_min,  timp_spawning_maxim), getRandomNumber(timp_procesare_min,  timp_procesare_maxim));
		
		return temp;
		
	}
	

}
