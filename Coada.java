
package ElementeMagazin;

import java.lang.Thread.State;

import java.util.ArrayList;

import Instrumente.*;

public class Coada{
	
	ArrayList<Client> sir = new ArrayList<Client>();
	
	public int timp_mediu_asteptare=0;
	public int timp_mediu_procesare=0; 
	public int timp_mediu_iesire=0;
	public int nr_clienti=0;
	public int nr_clienti_total=0;
	public ThreadCoada un;
	
	//multe nfrmatii relevante pentru functionarea cat mai optima a cozii fie pentru a stii cum sa intre clientii 
	//fie pentru a se calcula nr medii
	//o lista pentru toti clientii care stau in coad asi asteapta procesarea
	
	public void adauga(Client temp)
	{
		if(Magazin.spectime_min < TimerMagazin.timp_program && Magazin.spectime_max > TimerMagazin.timp_program) {
		timp_mediu_asteptare+=temp.timp_asteptare;
		timp_mediu_procesare+=temp.timp_procesare;
		nr_clienti_total++;
		}
		
		nr_clienti++;
		int salv = 0;
		for(Client sent : sir)
		{
			salv = salv + sent.timp_procesare;
		}
		temp.timp_iesire=salv;
		temp.nume= temp.nume + "_" +temp.timp_iesire;
		sir.add(temp);
		
	}
	
	//metoda pentru a adauga clientii in coada ce ii pregateste pt procesare
	//modifica timpii pentru a avea rezultatele medii
	
	public void scoate(Client x)
	{
		//sir.remove(x);
		x.nume="";
		nr_clienti--;
	}
	
	//aici scoatem clientii din coada
	
	public String toString(int i)
	{
		String temp1="Coada " + i + ":";
		
		for(Client sent : sir)
		{
			if(sent.nume != "") {
			temp1=temp1+" | "+sent.nume;
			}
		}
		temp1=temp1+"\n";
		return temp1; 
	}
	
	//metoda pt afisarea continutului unei cozi
	
	public String toStringMediu(int i)
	{
		String temp1="Coada " + i + " : ";
		float timpmediu1, timpmediu2, timpmediu3;
		
		if(nr_clienti_total == 0)
		{
			temp1=temp1+"Timp mediu asteptare: " +  "0 : Timp mediu procesare: "  +"0 : Timp mediu iesire: 0";
		}
		else
		{
			timpmediu1=(float)timp_mediu_asteptare/(float)nr_clienti_total;
			timpmediu2=(float)timp_mediu_procesare/(float)nr_clienti_total;
			timpmediu3=(float)timp_mediu_iesire/(float)nr_clienti_total;
			temp1=temp1+"Timp mediu asteptare: " + timpmediu1 +" : Timp mediu procesare: "+ timpmediu2 +" : Timp mediu iesire: "+ timpmediu3;
		}
		
		temp1=temp1+"\n";
		return temp1; 
	}
	
	//metoda pt calcularea timpilor medii
	
	public void proceseaza()
	{
		if(sir.isEmpty() == false) {
		for(Client auxc : sir)
		{
			
			//System.out.println(" PULAAAA \n");
			if((TimerMagazin.timp_program - 1) == auxc.timp_iesire) {
				Magazin.logToShow=Magazin.logToShow + TimerMagazin.timp_program + " : " +auxc.nume+" a inceput procesarea. \n";
				timp_mediu_iesire+=auxc.timp_iesire;
				System.out.println(TimerMagazin.timp_program +  " : " +auxc.nume+" a inceput procesarea \n");
			}
			if((TimerMagazin.timp_program - 1) == (auxc.timp_iesire + auxc.timp_procesare)) {
				Magazin.logToShow=Magazin.logToShow + TimerMagazin.timp_program + " : " +auxc.nume+" s-a terminat de procesat. \n";
				timp_mediu_iesire+=auxc.timp_iesire;
				System.out.println(TimerMagazin.timp_program +  " : " +auxc.nume+" a terminat procesarea. \n");
				scoate(auxc);
			}	
			//metda de procesare ce afiseaza in log evolutiile coziilor si produce scoaterea elementelor
		}
		}
	}
	
}
