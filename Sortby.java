

package Instrumente;

import ElementeMagazin.*;

import java.util.Comparator;

public class Sortby implements Comparator<Client>{
	
	public int compare(Client a, Client b) 
    { 
        return a.timp_asteptare - b.timp_asteptare; 
    } 
//clasa facuta special pentru a ajuta la sortarea dupa timpul de asteptare al clientilor
	//cu acesta putem avea sortati clientii dupa timpul de asteptare si asa intra in cozi dupa acest numar
}
