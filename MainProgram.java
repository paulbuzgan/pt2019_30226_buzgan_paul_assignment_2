package Interfata;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.Timer;

import ElementeMagazin.*;

import Instrumente.*;






public class MainProgram
{
    public static void main( String[] args )
    {
    	//cream inetrfata grafica
    			JFrame frame = new JFrame("Main Program:");
    			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    			frame.setSize(1000, 1200);
    			
    			//impartim in diferite zone cu diferite caracteristici
    			JPanel panel0 = new JPanel();
    			JPanel panel1_1 = new JPanel();
    			JPanel panel1_2 = new JPanel();
    			JPanel panel2 = new JPanel();
    			JPanel panel3 = new JPanel();
    			JPanel panel5_1 = new JPanel();
    			JPanel panel4 = new JPanel();
    			final JPanel panel5 = new JPanel();
    			final JPanel panel6 = new JPanel();
    			
    			JPanel p = new JPanel();
    			p.add(panel0);
    			p.add(panel1_1);
    			p.add(panel1_2);
    			p.add(panel2);
    			p.add(panel3);
    			p.add(panel4);
    			p.add(panel5_1);
    			p.add(panel5);
    			p.add(panel6);
    			
    			JLabel l0 = new JLabel("ELEMENTE DE CONTROL: ");
    			panel0.add(l0);
    			
    			
    			//construim zona pt timpul de apartie al clientilor
    			JLabel l1_1 = new JLabel("Timp minim aparitie clienti: ");
    		    Integer[] choices1 =  new Integer[30];
    		    for(int i=0; i<30; i++)
    		    {
    		    	choices1[i]=i+1;	    	
    		    }

    		    final JComboBox<Integer> cb1_1 = new JComboBox<Integer>(choices1);

    		    cb1_1.setVisible(true);
    			
    			panel1_1.add(l1_1);
    			panel1_1.add(cb1_1);
    			panel1_1.setLayout(new FlowLayout());
    			
    			
    			
    			JLabel l1_2 = new JLabel("Timp maxim aparitie clienti: ");
    		    Integer[] choices2 =  new Integer[30];
    		    for(int i=0; i<30; i++)
    		    {
    		    	choices2[i]=i+1;	    	
    		    }

    		    final JComboBox<Integer> cb1_2 = new JComboBox<Integer>(choices2);

    		    cb1_2.setVisible(true);
    			
    			panel1_1.add(l1_2);
    			panel1_1.add(cb1_2);
    			panel1_1.setLayout(new FlowLayout());
    			
    			
    			
    			//construim zona pt timpul de asteptare al clientilor
    			JLabel l2_1 = new JLabel("Timp minim asteptare clienti: ");
    		    Integer[] choices3 =  new Integer[40];
    		    for(int i=0; i<40; i++)
    		    {
    		    	choices3[i]=i+1;	    	
    		    }

    		    final JComboBox<Integer> cb2_1 = new JComboBox<Integer>(choices3);

    		    cb2_1.setVisible(true);
    			
    			panel1_2.add(l2_1);
    			panel1_2.add(cb2_1);
    			panel1_2.setLayout(new FlowLayout());
    			
    			
    			
    			
    			
    			JLabel l2_2 = new JLabel("Timp maxim asteptare clienti: ");
    		    Integer[] choices4 =  new Integer[60];
    		    for(int i=0; i<60; i++)
    		    {
    		    	choices4[i]=i+1;	    	
    		    }

    		    final JComboBox<Integer> cb2_2 = new JComboBox<Integer>(choices4);

    		    cb2_2.setVisible(true);
    			
    			panel1_2.add(l2_2);
    			panel1_2.add(cb2_2);
    			panel1_2.setLayout(new FlowLayout());
    			
    			JLabel l2_S = new JLabel("Selecteaza perioada pentru afisare timpi medii: ");
    		    Integer[] choicesS1 =  new Integer[300];
    		    for(int i=0; i<300; i++)
    		    {
    		    	choicesS1[i]=i;	    	
    		    }

    		    final JComboBox<Integer> cb2_S = new JComboBox<Integer>(choicesS1);

    		    cb2_S.setVisible(true);
    			
    			panel1_2.add(l2_S);
    			panel1_2.add(cb2_S);
    			panel1_2.setLayout(new FlowLayout());
    			
    			
    		    Integer[] choicesS2 =  new Integer[300];
    		    for(int i=0; i<300; i++)
    		    {
    		    	choicesS2[i]=i+1;	    	
    		    }

    		    final JComboBox<Integer> cb2_S2 = new JComboBox<Integer>(choicesS2);

    		    cb2_1.setVisible(true);
  
    			panel1_2.add(cb2_S2);
    			panel1_2.setLayout(new FlowLayout());
    			
    			//construim zona pt nr de cozi si timpul simularii
    			JLabel l3_1 = new JLabel("Timp simulare: ");
    		    Integer[] choices5 =  new Integer[300];
    		    for(int i=0; i<300; i++)
    		    {
    		    	choices5[i]=i+1;	    	
    		    }

    		    final JComboBox<Integer> cb3_1 = new JComboBox<Integer>(choices5);

    		    cb3_1.setVisible(true);
    			
    			panel2.add(l3_1);
    			panel2.add(cb3_1);
    			panel2.setLayout(new FlowLayout());
    			
    			
    			
    			JLabel l4_2 = new JLabel("Numar de cozi: ");
    		    Integer[] choices6 =  new Integer[10];
    		    for(int i=0; i<10; i++)
    		    {
    		    	choices6[i]=i+1;	    	
    		    }

    		    final JComboBox<Integer> cb4_2 = new JComboBox<Integer>(choices6);

    		    cb4_2.setVisible(true);
    			
    			panel3.add(l4_2);
    			panel3.add(cb4_2);
    			panel3.setLayout(new FlowLayout());
    			
    			
    			
    			int timpafisat = TimerMagazin.timp_program;
    			
 

    	        //adaugam unde se pun rezultatele in in Text Area
    	        
    	        JLabel l5_0 = new JLabel("Evolutia cozilor:                                                       Timpi medii cozi:                                                                  LogEvents: ");
    	        panel5_1.add(l5_0); 
    	        
    	        //text area pt afisarea outputurilor necesare vizualizarii temei

    	        final JTextArea ta1 = new JTextArea("Waiting for start...", 20, 27);
    	        ta1.setLineWrap(true);
    	        ta1.setWrapStyleWord(true);
    	        final JScrollPane s1 = new JScrollPane(ta1);
    	        
    	        panel5.add(s1); 
    	        
    	        
    	        final JTextArea ta2 = new JTextArea("Waiting for start...", 20, 27);
    	        ta2.setLineWrap(true);
    	        ta2.setWrapStyleWord(true);
    	        final JScrollPane s2 = new JScrollPane(ta2);
    	        
    	        panel5.add(s2);      

    	        
    	        
    	        final JTextArea ta3 = new JTextArea("Waiting for start...", 20, 27);
    	        ta3.setLineWrap(true);
    	        ta3.setWrapStyleWord(true);
    	        ta3.setEditable(false);
    	        final JScrollPane s3 = new JScrollPane(ta3);
    	        panel5.add(s3);  
    	        
    			
    	        //timpul real al programului si nr maxim de clineti in cozi ca timp
    	        JLabel l6 = new JLabel("Timp:  ");
    	        panel6.add(l6);
    	        final JTextArea timer = new JTextArea("  ", 1, 3);
    	        
    	        JLabel l7 = new JLabel("Peak time:  ");
    	        panel6.add(l7);
    	        final JTextArea timerpeak = new JTextArea("  ", 1, 3);
    	        
    	        
    			//cream butoane
    			JButton b1 = new JButton("START");
    			JButton b2 = new JButton("SCHIMBA NR COZI");
    			JButton b3 = new JButton("STOP");
    			
    			//buton pentru pornirea programului 
    			
    			b1.addActionListener(new ActionListener() {
    				
    				public void actionPerformed(ActionEvent but1) {
    					Magazin alfa = new Magazin(cb4_2.getSelectedIndex()+1, cb1_1.getSelectedIndex() + 1, cb1_2.getSelectedIndex()+15, cb2_1.getSelectedIndex()+1, cb2_2.getSelectedIndex()+15,  cb2_S.getSelectedIndex(), cb2_S2.getSelectedIndex()+1);
    	    	        
    	    	        alfa.Start(cb3_1.getSelectedIndex()+1);
    	    	        
    	    	        alfa.distribuie();
    				}

    			});
    			//buton resetare nr de cozi deschise
    			b2.addActionListener(new ActionListener() {
    				
    				public void actionPerformed(ActionEvent but2) {
    					
    	    	        
    	    	        Magazin.cozi_deschise=cb4_2.getSelectedIndex()+1;
    				}

    			});
    			//buton de stop
    			b3.addActionListener(new ActionListener() {
    				
    				public void actionPerformed(ActionEvent but3) {
    					
    	    	        
    	    	        Magazin.op=false;
    	    	        Magazin.logToShow=Magazin.logToShow + "\n STOPPED BY BUTTON ";
    				}

    			});

    			//un timer ce face refresh la interfata grafica pentru a se afisa
    			//elemnete din interfata cu o frecventa de un heartz
    			//aici se updateaza variabile si cheama functiile cand trebuie executate
    			Timer timerRefresh= new Timer(1000, new ActionListener() {
    				
    				public void actionPerformed(ActionEvent e) {
    					int timpafisat= TimerMagazin.timp_program;
    					Magazin.distribuie();
    					Magazin.StartCaserii();
    					timer.setText(Integer.toString(timpafisat));//facem operatia
						panel6.add(timer);
						String stare = "";
						String mediu = "";
						int j=1, aux1=0;
						for(Coada a : Magazin.Caserii)
						{
							stare=stare+a.toString(j);
							mediu=mediu+a.toStringMediu(j);
							aux1=aux1+a.nr_clienti;
							j++;
						}
						
						if(Magazin.peak < aux1)
						{
							Magazin.peak=aux1;
							Magazin.peaksave=TimerMagazin.timp_program;
						}
						ta1.setText(stare);
						panel5.add(s1);
						
						ta2.setText(mediu);
						panel5.add(s2);
						
						ta3.setText(Magazin.logToShow);
						panel5.add(s3);
						
						timerpeak.setText(Integer.toString(Magazin.peaksave));//facem operatia
						panel6.add(timerpeak);

						
    				}



    			});
    			
    			timerRefresh.start();
    			
    			panel4.add(b1);
    	        panel4.add(b2);
    	        panel4.add(b3);
    	        
    	         timpafisat= TimerMagazin.timp_program;
				timer.setText(Integer.toString(timpafisat));
				panel6.add(timer);
    	        
    	        timer.update(timer.getGraphics());
    	        
    	        panel6.add(timer);



    			//adaugam butoane
    	        
    	      

    	        //terminam de facut interfata grafica
    			p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
    	   		frame.setContentPane(p);
    	   		frame.setVisible(true);
    }
}
